# Makefile for creating shibari figures

markdown != find . -name '*.md'
latex != find . -name '*.tex'

html = $(patsubst %.md,%.html,$(markdown))
pdf = $(patsubst %.tex,%.pdf,$(latex))
png = $(patsubst %.tex,%.png,$(latex))

all: $(html) $(pdf) $(png)

$(html): %.html: %.md
	pandoc -c "/shibari/shibari.css" --metadata pagetitle="`grep -Em1 '^#\s+.*$$' $< | sed -Ee 's/^#\s+//'`" -s -f markdown -t html5 -o $@ $<

$(pdf): %.pdf: %.tex
	pdflatex -output-directory $(@D) -interaction=nonstopmode $<

$(png): %.png: %.pdf
	convert -flatten -density 120 $< $@
