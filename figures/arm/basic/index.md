# Basic armbinder

## Description

This is a basic way to tie the arms to the back. It makes multiple use of the figure 8 inline double column tie. It can be tied with or without the elbow wraps, whatever feels more convenient.

## Instructions

#. Position the arms parallel to each other on the back, slightly pulling the shoulders back.
#. Start with the center bight in the middle of the back, between the shoulders, and make a wrap around the shoulders / upper part of the body from A to A.
#. Bull the rope ends through the center bight, forming a lark's head (ABOK #5).
#. Make a second wrap around the shoulders in the opposite direction from B to B, closely under the first one.
#. Pull the running ends through the loop formed by the wraps A-B on the other side.
#. Make a half hitch (ABOK #48) by crossing down on top of the two wraps, leaving a loop over them, above behind them, pulling them through the loop and tightening up. This should fix the upper wraps, and create the following pattern:

	![Initial wraps around the shoulders.](shoulder.png)

#. Run the rope ends down towards the hands, making a 90° turn over the wrists and rum them over the left forearm.
#. Make a wrap around the left forearm, coming back behind it, and cross over the vertical strands just above the 90° turn.
#. Make a wrap around the right forearm, first in front of it, then back behind it, and behind the vertical strands, above the previous turn.
#. Repeat the pattern, forming a figure 8 by first making a wrap around the left forearm, then around the right forearm, twice more, until you have three wraps around each arm.
#. Cross the rope ends behind the vertical strands and then down through all three wraps, parallel to the left arm.
#. At the lower end of wraps, reverse direction, and run the rope ends upwards through the wraps on the other side, parallel to the right arm.
#. Run the rope ends up along the center of the back, passing under the horizontal wraps towards the left shoulder A-B.
#. Reverse direction and cross down again, on top of the horizontal wraps towards the left shoulder A-B.
#. Run the rope ends down towards the elbows, and make a 90° turn to the left to run them over the left arm above the elbow.
#. Repeat the pattern from the forearms, making three wraps around each arm, each time forming a figure 8, such that the previously placed vertical strands pass through the wraps on the right arm.
#. Pass the running ends down through the wraps around the left arm.
#. Reverse direction, and pass the running ends up through the wraps around the right arm.
#. Reverse direction again, and pass the running ends down again through the wraps around the left arm.
#. Continue downwards below the elbows, and make a 90° turn to the left to run them over the left forearm.
#. Repeat the pattern from the upper arms and the forearms, making three wraps around each arm, each time forming a figure 8, such that the previously placed vertical strands pass through the wraps on the right arm.
#. Pass the running ends down through the wraps around the left forearm.
#. Reverse direction, and pass the running ends up through the wraps around the right forearm.
#. Lock the tie with a half hitch (ABOK #48) on the three vertical strands. The result will look like this:

	![Final position with three sets of wraps.](final.png)

#. Steps 19 to 23 may also be omitted, hence not making a second set of wraps around the forearms, and locking the tie with a half hitch over the wraps on the upper arms. This will create the following simpler figure:

	![Final position with two sets of wraps.](simple.png)

## References

* <http://crash-restraint.com/ties/41>
