# Basic TK with only lower kannukis

## Description

The following TK is very basic and may be used as a starting point for suspension, but also for pure floor bondage. It has been taught at a workshop, and the following instructions are created based on the ones given during the course. It has the following properties:

* The construction is very simple. It combines the basic techniques of rope crossings and frictions - X, L and U friction.
* It may be used as a starting point to which further decorations can be added.
* If supplemented with upline ropes, it may be used for different positions of suspension - either from the back or from the side.
* Kannukis are placed only on the lower chest wraps, not on the upper chest wraps. This reduces the risk of nerve injuries since there are no kannukis under the armpits.
* On the back it is very compact, with at most a short stem between the arms and the chest wraps.

## Instructions

#. Place your partner's arms in the box tie position.
#. Tie a single column tie (SCT) around your partner's arms. Make sure that it is comfortable and not too tight.
#. Go to the right arm A, slightly underneath the shoulder, and lay the ropes around the upper part of the chest to the left arm B.
#. On the back, continue horizontally towards the right arm C, such that the new wrap will be immediately under the previous wrap on the arm. Note that by doing so, you will cross over the previous strand SCT-A. Keep this crossing on the back, not on the arm.
#. Go once more around the upper part of the chest from C to D, closely under the strand A-B, in the same direction.
#. On the back, grab the strand SCT-A and pull it to create a right angle on the center of the back. The vertical part ("stem") should be rather short (around 10cm).
#. Lay the rope ends coming from D towards the right over the vertical stem, then upwards underneath the strand B-C and the horizontal rope which continues towards A. The initial rope crossing of the first two wraps should now be in the center of the back.
#. Cross down over the strands going to B and D.
#. Continue towards the right under the vertical stem towards the right arm E.
#. The pattern on the back should now resemble the shape of the X-friction:

	![Basic TK - X-friction.](back1.png)

#. Continue over the right arm E towards the front, keeping the newly put rope just under the strand C-D on the right arm.
#. After crossing over the arm, branch off and continue over the lower part of the chest towards the left arm F.
#. On the arm, lay the rope ends F again closely under the strand C-D.
#. Make sure that the tension of the new wrap E-F is about 30% less than that of the wraps A-B and C-D.
#. On the back, cross horizontally from F to G, underneath the stem and immediately under the X-friction knot.
#. Continue again over the right arm G, now closely under the strand E-F over the lower part of the chest, towards the left arm H.
#. Make sure that the tension of the new wrap G-H is about the same as that of the previous wrap E-F.
#. Cross over the left arm H to the back, still closely under the previous wrap.
#. Reaching the center of the back, cross under the stem to the right, then over it to the left.
#. Cross the strands going towards B, D, F and H, first upwards underneath the old strands, then downwards over them.
#. Continue to the right under the stem towards I.
#. Now the pattern on the back should follow the L-friction shown in the following diagram:

	![Basic TK - first L-friction.](back2.png)

#. Pass the ropes to the front under the right arm I, such that at the front you end up under the wraps A-B and C-D, but over E-F and G-H.
#. Slightly turn the wraps E-F and G-H towards the back before crossing downwards in front of them with the free rope ends. This prevents the wraps E-F and G-H from "falling forward" when pulling the rope ends over them.
#. Pass the rope ends back under the right arm towards J.
#. Pull the rope ends to increase the tension of the wraps E-F and G-H, by about 15% of the tension of the wraps A-B and C-D.
#. On the back, cross under the stem towards the left, then over the stem towards the right.
#. Cross upward under the strands going to A, C, E, G, I and J, then back downwards over them.
#. Cross once more under the stem towards the left arm K.
#. Now the pattern on the back should follow the following diagram, which is essentially the mirror image of the preceding L-friction:

	![Basic TK - second L-friction.](back3.png)

#. Pass the ropes to the front under the left arm K, such that at the front again you end up under the wraps A-B and C-D, but over E-F and G-H.
#. As with the right side, slightly turn the wraps E-F and G-H towards the back before crossing downwards in front of them with the free rope ends.
#. Pass the rope ends back under the left arm towards L.
#. Pull the rope ends to increase the tension of the wraps E-F and G-H, by another 15% of the tension of the wraps A-B and C-D. Now all wraps in the front should have the same tension.
#. On the back, cross under the stem towards the right, then upward over the strands going to A, C, E, G, I and J.
#. Cross downward under the horizontal strands on the right, then left over the stem.
#. Cross upwards under the strands going towards B, D, F, H, K and L, then downwards over them.
#. Finally, cross once more under the stem towards the right.
#. The pattern obtained in the last steps should have the form of a U-friction:

	![Basic TK - U-friction.](back4.png)

	![Basic TK - final front view.](front.png)

#. There are different possibilities to continue from here:
	#. Finish the TK by attaching the rope ends, then attach an upline for suspension to the strands going to B, D, F, H, K, L and A, C, E, G, I, J. Make sure that it is attached to these horizontal strands, so that the weight is carried by the wraps around the chest - not on the vertical stem connecting the SCT.
	#. Add decorative ropes on the front.
