# Star back harness

## Description

This harness makes extensive use of the double coin knot as a decorative element. It features a star shaped figure on the back. Despite being mostly decorative, it also serves for tying the arms to the back, in a crossed position, though not in a very tight manner.

## Instructions

#. Start by laying the center bight around the back of the neck, connecting A and B.
#. On the front, tie a double coin knot (ABOK #1428) in the middle of the upper half of the chest.
#. Pull the two outer loops of the double coin knots to enlarge them towards the sides, such that they reach almost to the armpits.
#. In the open ends of the rope, tie another double coin knot (ABOK #1428) in the middle of the chest, approximately 10cm below the first one.
#. With the open ends, form a loose loop on each side, surrounding the lower half of the chest, then pull the open ends through the two loops formed from the first double coin knot.
#. Run the open ends over shoulders towards C and D. The result should look as follows:

	![Front view - before adding arm ties](first.png)

#. On the back, tie another double coin knot (ABOK #1428) approximately 10cm below the neck.
#. Run the rope ends towards the front underneath the armpits along E and F, through the lower chest loops and back under the armpits towards G and H.
#. Pull the ropes tight such that the front loops become tight. The crossing point between the front and back ropes should be under the armpits.
#. Fix the tension of the rope by tying a square knot (ABOK #1204) in the center of the back, approximately 10cm below the double coin knot.
#. On each open end, form a bight, and run the bights upwards under the strands to E and F, downwards over the same strands and downwards under the strands to G and H.
#. Pull the rope ends through the bights and pull tight, such that the upper and lower strands come close to each other and a tight knot is formed around the center square knot.
#. Approximately 10cm below the center knot, tie another double coin knot (ABOK #1428) in the rope ends. Now you should have this figure on the back:

	![Back view - upper part before adding arm ties](second.png)

#. Run the rope ends down the back across I and J, and between the legs to the front towards K and L.
#. On the front, run the rope ends through the outer loops of the lower double coin knot.
#. Run the rope ends towards the back, under the arms, below the strands which have been placed there before.
#. Run the rope ends to the front again, now crossing over the arms.
#. Run the rope ends upwards, crossing underneath the strands coming from the lower double coin knot.
#. Going through the loops on the lower part of the chest, run the rope ends towards the back again, crossing over the arms.
#. Run the rope ends downwards behind the bights E-G and F-H, thus forming a bight around each of these.
#. Run the rope ends to the front again, crossing over the arms.
#. Run the rope ends downwards, back through the loops on the lower part of the chest and under the two strands coming from the lower double coin knot on either side.
#. Pass the rope ends over the arms towards the back at M and N. You should have obtained this figure on the front and arms:

	![Front view - after adding arm ties](front.png)

#. On the back, coming from M and N, make two loops around the strands to E-G and F-H, by first passing upwards under these strands, then downwards over them, and repeating this.
#. On either side, make a half hitch (ABOK #48) by first passing upwards over the two horizontal strands, keeping a bight in front of them, then downwards below them, and pulling the rope ends through the bight, towards O and P. The result should look like this:

	![Back view - upper part after adding arm ties](upper.png)

#. Cross the forearms on the back, over the strands running from I-J to K-L.
#. Create a pair of loops around the wrists on either side, by first passing the rope ends from O and P behind both arms, then crossing upwards in front of the arms, and repeating this.
#. Cross the ropes above the wrists and behind the strands going to I-J-O-P, pulling the loops around the wrists tight.
#. Run the rope ends down in front of the forearms, but behind the hands.
#. Make a granny knot (ABOK #1206) around the strands going to K-L, by first making a half knot in front of these strands and then another one behind them.
#. Run the rope ends upwards again, behind the hands and in front of the forearms.
#. Make a granny knot (ABOK #1206) around the strands going to I-J-O-P, by first making a half knot in front of these strands and then another one behind them.
#. Run the rope ends down once more in front of the forearms, but behind the hands.
#. To lock the figure, make a granny knot (ABOK #1206) around the strands going to K-L, by first making a half knot behind these strands and then another one in front of them. The result should look like this:

	![Back view - lower part](lower.png)

NB! For the steps 16 to 25, it is useful to complete these steps first on one side, and then do them on the other side, such that the concluding half hitch (ABOK #48) will hold the tension of the rope.

## References

* <http://www.knottyboys.com/videos/Harnesses/Star_Back.wmv>
