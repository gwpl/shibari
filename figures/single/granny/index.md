# Granny knot single column tie

## Description

The granny knot single column tie is very similar to its [close relative based on the reef knot](../reef/). It might be a bit less stable, depending on the direction of the pulling force on the long end of the rope, and can be untied as easily. In general, it can be untied more easily that the [Somerville bowline](../somerville/). It is based on the granny knot (ABOK #1206).

## Instructions

#. Start with (at least) two wraps of rope around the column, leaving about 20cm at the short end Y.
#. Wrap the short end once around the wraps - first over the long ends, then pull through under the wraps.
#. Create a loop in the long ends X, which points *in the same direction* as the loop created by the short end Y.
#. Pull the shorts end Y from above through the loop in the long ends X.
#. Pull both ends X and Y tight. You should have this result now:

	![Granny knot single column tie](final.png)
