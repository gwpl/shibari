# Under-ribs karada

## Description

This tie is a decorative body harness, which serves as an alternative to the usual karada. Unlike the latter, it does not require tying between the legs, and may thus be more comfortable.

## Instructions

#. Start with the center bight and lay the ropes around the narrowest part of the waist, just below the ribs, through A and B.
#. Pull the rope ends through the center bight, forming a lark's head (ABOK #5), and make sure that it is tight enough that it will not slide up.
#. Lock the tension of the waist rope by making a half hitch (ABOK #48).
#. Go straight up across the center of the chest and make an overhand knot in both rope ends together just below the base of the neck.
#. Split the rope ends, and run one of them over each shoulder C and D to the back.
#. A bit below the neck, connect the ropes with a square knot (ABOK #1204) in the center of the spine.
#. Run the ropes separately to the front again, passing just below the armpits E and F on either side.
#. On upper part of the chest, pass the rope ends through the loop formed by the vertical strands between the lower half hitch and the upper overhand knot.
#. Pull the ropes outward so that the central loop opens up.
#. Before returning to the back at G and H, cross over the strands coming from E and F, to create a friction which will keep the loop in place.
#. On the back, cross the ropes from G and H to I and J, and run them to the front again.
#. On the lower part of the chest, pass the rope ends through the central loop again.
#. Pull the ropes outward so that the central loop will form a hexagonal shape.
#. Before returning to the back at K and L, cross over the strands coming from I and J, to create a friction which will keep the loop in place.
#. The front should now resemble the upper part of a tortoise karada:

	![Front view](front.png)

#. On the back, lock the two ends with a square knot, so that the final stage will look as in the following figure:

	![Back view](back.png)

#. If there is any unused excess rope, it may be used to create a decorative pattern on the back. For example, a crossing pattern as in the video:

	![Example of a decorative pattern](pattern.png)

## References

* <http://crash-restraint.com/ties/34>
